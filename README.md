Los pasos a seguir para utilizar el Playbook son los siguientes:

1. Usando un Docker Bastion como nuestro servidor principal 
se consigue que se enlacen los demas contenedores (Sales, Accounting, HR).

2. Se utiliza SSH para manipular servicios de red y se genera un network
con un nombre por defecto, en este caso n1.

3. Al realizar las conexiones correspondientes se configura un .yml, 
dentro de este se agregan las instrucciones a realizar en los distintos
servidores.

4. Usando ansible se hace uso del comando ansible-playbook (nombre_playbook).yml
para hacer uso del mismo, y todo deberia funcionar, al final en este ejemplo se
deberia crear una carpeta alumnos en cada contenedor con 2 archivos.
